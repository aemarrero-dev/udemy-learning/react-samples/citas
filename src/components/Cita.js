import React from 'react';
import PropTypes from 'prop-types'; //Para documentar el codigo

//Recibe la cita que mostrará en la seccion derecha (citas creadas)
const Cita = ({cita, eliminarCita}) => (
    <div className = "cita">
        <p>Mascota: <span>{cita.mascota}</span></p>
        <p>Dueño: <span>{cita.propietario}</span></p>
        <p>Fecha: <span>{cita.fecha}</span></p>
        <p>Hora: <span>{cita.hora}</span></p>
        <p>Síntomas: <span>{cita.sintomas}</span></p>

        <button
            className = "button eliminar u-full-width"
            /* Si colocamos solo eliminarCita llamaria la funcion.
               Pero la colocamos como arrow function para esperar que el evento se ejecute. */
            onClick = {() => eliminarCita(cita.id) } 
        >Eliminar &times;</button>
    </div>
);

Cita.propTypes = {
    cita : PropTypes.object.isRequired,
    eliminarCita : PropTypes.func.isRequired 
}
 
export default Cita;