import React, {Fragment, useState} from 'react';
//import uuid from 'uuid/v4'; //Segun el curso pero no me funcionó
import {v4 as uuid} from 'uuid'; //instalamos npm i uuid
import PropTypes from 'prop-types'; //Para documentar el codigo

const Formulario = ({crearCita}) => {
    
    //State de Citas para recibir datos ingresados por el usuario.
    //actualizarCita sera la funcion que modificara este State
    const [cita, actualizarCita] = useState({
        mascota: "",
        propietario: "",
        fecha: "",
        hora: "",
        sintomas:""
    });
    
    //Funcion que se ejecutara cuando usuario ingrese los datos. Se pasa evento evt
    /* evt.target referencia al elemento o input. evt.target.name referencia al name del input 
       evt.target.value referencia el valor ingresado.
    */ 
    const actualizarState = evt => {
        actualizarCita({
            ...cita, //Se hace una copia del state (datos ingresados)
            [evt.target.name] : evt.target.value
        })
        //console.log(evt.target);
    }

    //State para gestionar mensaje de error cuando datos son vacios
    //actualizarError sera la funcion que modificara este State
    const [mensajeError, actualizarError] = useState(false);

    //Extraigo los valores (Destructuring)
    const {mascota, propietario, fecha, hora, sintomas} = cita;

    //Cuando se presiona boton de enviar cita
    const submitCita = evt => {
        evt.preventDefault(); //Detenemos propagacion de evento
        
        //Validacion de datos
        if(mascota.trim()==='' || propietario.trim() === '' || 
            fecha.trim()==='' || hora.trim()==='' || sintomas.trim() === '') {
            actualizarError(true);
            return;
        }

        //Restauramos a false para que no se visualize mensaje de error
        actualizarError(false);

        //Asignar un ID - instalamos npm i uuid
        cita.id = uuid();
        console.log(cita);
        
        //Crear la cita
        crearCita(cita);

        //Reiniciar el form
        actualizarCita({
            mascota: "",
            propietario: "",
            fecha: "",
            hora: "",
            sintomas:""
        })

        console.log('Enviando datos.')
    }
    
    //RETURN
    return ( 
        <Fragment>
             <h2>Crear Cita</h2>
             
             {
                mensajeError 
                ? <p className = "alerta-error">Todos los campos son obligatorios</p>
                : null
             }
             
             { /*Formulario*/ }
             <form onSubmit = {submitCita} >
                <label>Nombre Mascota</label>
                <input
                    type = "text"
                    name = "mascota"
                    className = "u-full-width"
                    placeholder = "Nombre Mascota"
                    onChange={actualizarState}
                    value = {mascota}
                />
                <label>Nombre Dueño</label>
                <input
                    type = "text"
                    name = "propietario"
                    className = "u-full-width"
                    placeholder = "Nombre Dueño de la Mascota"
                    onChange={actualizarState}
                    value = {propietario}
                />
                <label>Fecha</label>
                <input
                    type = "date"
                    name = "fecha"
                    className = "u-full-width"
                    onChange={actualizarState}
                    value = {fecha}
                />
                <label>Hora</label>
                <input
                    type = "time"
                    name = "hora"
                    className = "u-full-width"
                    onChange={actualizarState}
                    value = {hora}
                />
                <label>Sintomas</label>
                <textarea
                    name = "sintomas"
                    className = "u-full-width"
                    onChange={actualizarState}
                    value = {sintomas}
                ></textarea>
                <button
                    type = "submit"
                    name = "sintomas"
                    className = "u-full-width button-primary"
                >Agregar Cita</button>
             </form>
        </Fragment>
       
     );
}

//Documentacion de este componente
Formulario.propTypes = {
    crearCita : PropTypes.func.isRequired //Funcion de entrada que recibe este componente
}

export default Formulario;