import React, {Fragment, useState, useEffect} from 'react';
import Formulario from './components/Formulario';
import Cita from './components/Cita';

//Usamos skeleton como framework css.
function App() {
  
  /* LocalStorage mantiene la informacion si se cierra navegador.
    SessionStorage no mantiene la informacion al cerrarse el navegador.
    Se usa JSON.parse porque en localStorage solo se maneja Strings
  */
  let citasIniciales = JSON.parse(localStorage.getItem('citas'));
  if(!citasIniciales) {
    citasIniciales = [];
  }

  //Arreglo que almacenara las citas creadas. AL inicio se pasaba vacio [].
  //Ahora obtengo citas segun lo encontrado en localStorage
  const [citas, guardarCitas] = useState(citasIniciales);

  /* useEffect es util para ejecutar ciertas operaciones cuando un State o componente este listo
     o cambia. Siempre es un arrow function. 
     Para que se ejecute una sola vez este useEffect se debe colocar [] de parametro. 
     Con [citas], el useEffect se ejecutara cada vez que citas cambie.
  */
  useEffect( () => {
    let citasIniciales = JSON.parse(localStorage.getItem('citas'));
    if(citasIniciales) {
      localStorage.setItem('citas', JSON.stringify(citas));
    } else {
      localStorage.setItem('citas', JSON.stringify([]));
    }
  }, [citas]);
  
  //Funcion que toma las citas actuales y agrega la nueva
  const crearCita = cita => {
    guardarCitas([...citas, cita]); //Se hace una copia del array existente y se agrega la cita
  }

  //Funcion que elimina una cita por su id
  const eliminarCita = id => {
    //Usamos !== para obtener los elementos con id distinto al ingresado como parametro
    const nuevasCitas = citas.filter(cita => cita.id !== id);
    guardarCitas(nuevasCitas);
  }

  //Mensaje condicional
  const titulo = citas.length === 0 ? 'No Existen Citas' : 'Administre tus Citas';
  
  //IMPORTANTE: la forma como fluyen los datos del padre al hijo es a traves de los props
  return (
    <Fragment>
      <h1>Administrador de Pacientes</h1>
      
      <div className = "container">
        <div className = "row">
          
          <div className = "one-half column">
            <Formulario //Llamada a componente Formulario
              crearCita = {crearCita}
            />
          </div>

          <div className = "one-half column">
            <h2>{titulo}</h2>
            {citas.map(citaObj => ( //Iteramos las citas y la guardamos en objeto Cita su id e info
              <Cita //Llamada a componente Cita
                key = {citaObj.id}
                cita = {citaObj}
                eliminarCita = {eliminarCita}
              />    
            ))}

          </div>
        </div>
      </div>
    </Fragment>
    
  );
}

export default App;

/* DEPLOYMENT DE ESTE PROYECTO
1. npm run build. Nos genera una carpeta build en nuestro proyecto.
2. Usaremos Netlify para desplegar nuestro proyecto de manera publica en la web
3. Podemos desplegar a traves de Git o copiando la carpeta build en Netlify
*/