APUNTES CURSO UDEMY - NOCIONES DE NODEJS, NPM

1. Instalar NodeJS, validar version con node -v y npm -v.
2. Ejecutar npx create-react-app nombre_proyecto.
3. nombre_proyecto debe estar en minusculas y sin espacios.
4. create-react-app es una herramienta para crear proyectos en react.
5. webpack es un bundler (paquete) de modulo de aplicaciones javascript modernas.
6. webpack procesa la aplicacion y mapea todas las dependencias de un modulo,
con esto crea uno o varios paquetes o bundlers.
7. Las dependencias de webpack son estaticas. js, imagenes, css, ts (typescript), etc.
Ademas posee un servidor local con LiveReload que actualiza en caliente los cambios.
8. Una vez terminado el comando 'npx create-react-app', nos ubicamos en basicos con:
'cd basicos' y luego 'npm start'.
Algunos comandos basicos:
a. npm start (start the server)
b. npm run build (bundles the app into static files for production)
c. npm test (start test runner)
d. npm run eject (Watch Out!: removes this tool and copies build dependencies)
*/

//ESTRUCTURA DE PROYECTO

1. package.json define las dependencias.
2. node_modules contiene las dependencias.
3. public contiene recursos publicos (index.html, imagenes, iconos, etc.)

JSX: javascript que combina codigo html, css. Muy usado en React.

//REACT HOOKS
Disponibles desde la version 16.8 de React. Permiten actualizar el State sin
necesidad de crear un Class Component. Anteriormente existian 2 tipos de componentes:
Class Components (usado para cambiar el State) y Stateless Functional Component (usado para mostrar el State). 

Existen 2 tipos de Hooks:
Basicos (useState, useEffect)
Avanzados (useContext, useRef, useReducer, useCallback, useMemo)

En el State se colocara todo lo que va reaccionar a ciertas acciones de los usuarios. Por ejemplo,
datos de un formulario cuando usuario escriba, un carrito de compras, etc.

IMPORTANTE: El State NO se puede modificar o tratar directamente. Para eso estan las funciones definidas en la
declaracion del State.

--------------------------------------------------------------------------------------------------

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

### ACCESO A APP
https://citas-react-sample.netlify.app/ 